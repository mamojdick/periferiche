Cisco spa5x5 Localization Release for version 7.5.6 r2
******************************************************************
*1. File description:
******************************************************************
This Folder includes 21 languages xml script:
File				Language
spa50x_30x_en_v756.xml		English_United_States
spa50x_30x_es_v756.xml		Spanish_Spain
spa50x_30x_de_v756.xml		German_Germany
spa50x_30x_it_v756.xml		Italian_Italy
spa50x_30x_fr_v756.xml		French_France
spa50x_30x_pt_v756.xml		Portuguese_Portugal
spa50x_30x_nl_v756.xml		Dutch_Netherlands
spa50x_30x_dk_v756.xml		Danish_Denmark
spa50x_30x_se_v756.xml		Swedish_Sweden
spa50x_30x_no_v756.xml		Norwegian_Norway
spa50x_30x_es_mx_v756.xml		Spanish-Mexico
spa50x_30x_he_v756.xml		Hebrew
spa50x_30x_tr_v756.xml		Turkish
spa50x_30x_sk_v756.xml		Slovak
spa50x_30x_ru_v756.xml		Russian
spa50x_30x_hu_v756.xml		Hungarian
spa50x_30x_hr_v756.xml		Croatian
spa50x_30x_fi_v756.xml		Finnish
spa50x_30x_cz_v756.xml		Czech
spa50x_30x_bg_v756.xml		Bulgarian
spa50x_30x_pl_v756.xml		Polish

spa50x_30x_v756_r2.zip	is the .zip file includes these 21 languages xml
*******************************************************************
*2. How to download the xml:
*******************************************************************
*2.1. SIP Mode:
*******************************************************************
2.1.1 Place these .xml file into the appropriate location on the tftp server.

2.1.2 fill in the Reginal -> Miscellaneous -> Dictionary Server Script with the following script, for example:

serv=tftp://192.168.1.4/;d0=English-US;x0=spa50x_30x_en_v756.xml;d1=Portuguese;x1=spa50x_30x_pt_v756.xml;d2=Danish;x2=spa50x_30x_dk_v756.xml;d3=Swedish;x3=spa50x_30x_se_v756.xml;d4=Norwegian;x4=spa50x_30x_no_v756.xml;d5=Spanish-Mexico;x5=spa50x_30x_es_mx_v756.xml;d6=Spanish;x6=spa50x_30x_es_v756.xml;d7=German;x7=spa50x_30x_de_v756.xml;d8=Dutch;x8=spa50x_30x_nl_v756.xml;d9=Italian;x9=spa50x_30x_it_v756.xml;d10=French;x10=spa50x_30x_fr_v756.xml;


2.1.3 Some information for script:

	The d0,x0 pair is still reserved for English, but is optional.  If it is not specified, the built-in English-US dictionary will be used.

******************************************************************
	
